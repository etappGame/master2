package com.example.nicolas.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

public class QuizActivity extends FirebaseActivity {

    Button btnResponder;
    QuizEvento quiz;
    RadioButton resp1;
    RadioButton resp2;
    RadioButton resp3;
    RadioButton resp4;
    TextView pergunta;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        btnResponder = (Button) findViewById(R.id.btnResponder);
        resp1 = (RadioButton) findViewById(R.id.rbResposta1);
        resp2 = (RadioButton) findViewById(R.id.rbResposta2);
        resp3 = (RadioButton) findViewById(R.id.rbResposta3);
        resp4 = (RadioButton) findViewById(R.id.rbResposta4);
        pergunta = (TextView) findViewById(R.id.pergunta);


        btnResponder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(QuizActivity.this,activity_index.class);
                startActivity(i);
            }
        });

        inicializarFirebase();
        final String idQuiz = getIntent().getExtras().getString("quizUUID");
        carregarQuiz(idQuiz);


    }

    private void carregarQuiz(final String uuidQuiz){


        databaseReference.child("Quiz")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        String id = null;

                        for(DataSnapshot object : dataSnapshot.getChildren()){
                            id = String.valueOf(object.child("uuid").getValue());


                            if(id.equals(uuidQuiz)){

                                quiz = object.getValue(QuizEvento.class);
                                resp1.setText(quiz.getAlt1());
                                resp2.setText(quiz.getAlt2());
                                resp3.setText(quiz.getAlt3());
                                resp4.setText(quiz.getAlt4());
                                pergunta.setText(quiz.getPergunta());

                                break;
                            }
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }
}
